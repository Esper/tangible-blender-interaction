import socket
import SocketServer


class MyUDPHandler(SocketServer.BaseRequestHandler):

    def handle(self):
        data = self.request[0].strip()
        socket = self.request[1]

        print "{} wrote: ".format(self.client_address[0])
        print data

        socket.sendto(data.upper(), self.client_address)

if __name__ == "__main__":
    # HOST, PORT = socket.gethostname(), 8668
    HOST, PORT = "192.168.1.13", 8668

    server = SocketServer.UDPServer((HOST, PORT), MyUDPHandler)

    print "listening on {}".format(HOST)
    server.serve_forever()
