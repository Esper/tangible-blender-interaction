from threading import Thread

bl_info = {"name": "My Test Addon", "category": "Object"}

import bpy
from math import radians
import socket

# HOST, PORT = socket.gethostname(), 8668
HOST, PORT = '192.168.1.14', 8668

sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

sock.sendto(bytes("\n", 'UTF-8'), (HOST, PORT))

selected = bpy.context.selected_objects

polling = True

for obj in selected:
    obj.rotation_euler.x = radians(0.0)
    obj.rotation_euler.y = radians(0.0)
    obj.rotation_euler.z = radians(0.0)


def start_polling():
    global polling

    try:
        while True:
            received = sock.recv(1024)

            received = received.decode('UTF-8')
            x, y, z = tuple(received.split(', '))

            #print "Sent:     {}".format(data)
            print("Received: {}, {}, {}".format(x, y, z))

            for obj in selected:
                obj.rotation_euler.x = radians(float(x))
                obj.rotation_euler.y = radians(float(y))
                obj.rotation_euler.z = -radians(float(z))

    except KeyboardInterrupt:
        polling = False

Thread(target=start_polling).start()


def register():
    pass	


def unregister():
    pass