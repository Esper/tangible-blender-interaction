import socket
import sys

# HOST, PORT = socket.gethostname(), 8668
HOST, PORT = '192.168.1.13', 8668
data = " ".join(sys.argv[1:])

sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)


try:
    sock.sendto(data + "\n", (HOST, PORT))

    while True:
        received = sock.recv(1024)

        #print "Sent:     {}".format(data)
        print "Received: {}".format(received)

except KeyboardInterrupt:
    sock.sendto("exit", (HOST, PORT))

    sock.shutdown(0)
    sock.close()
