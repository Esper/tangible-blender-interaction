#!/usr/bin/env python2.7

import smbus
import math
import socket
import SocketServer
import time
from threading import Thread

# Power management registers

power_mgmt_1 = 0x6b
power_mgmt_2 = 0x6c

time_diff = 0.01

def read_byte(adr):
    return bus.read_byte_data(address, adr)

def read_word(adr):
    high = bus.read_byte_data(address, adr)
    low = bus.read_byte_data(address, adr+1)
    val = (high << 8) + low
    return val

def read_word_2c(adr):
    val = read_word(adr)
    if (val >= 0x8000):
        return -((65535 - val) + 1)
    else:
        return val


def dist(a,b):
    return math.sqrt((a*a)+(b*b))


def get_y_rotation(x, y, z):
    radians = math.atan2(x, dist(y, z))
    return -math.degrees(radians)


def get_x_rotation(x, y, z):
    radians = math.atan2(y, dist(x,z))
    return math.degrees(radians)


def get_z_rotation(x, y, z):
    radians = math.atan2(z, dist(x, y))
    return math.degrees(radians)

bus = smbus.SMBus(1) # or bus = smbus.SMBus(1) for Revision 2 boards
address = 0x68       # This is the address value read via the i2cdetect command

# Now wake the 6050 up as it starts in sleep mode
bus.write_byte_data(address, power_mgmt_1, 0)


def read_gyro_data():
    gyro_xout = (read_byte(0x43) << 8) + read_byte(0x44)
    gyro_yout = (read_byte(0x45) << 8) + read_byte(0x46)
    gyro_zout = (read_byte(0x47) << 8) + read_byte(0x48)

    # print gyro_xout

    gyro_x_scaled = twos_compliment(gyro_xout) / 131
    gyro_y_scaled = twos_compliment(gyro_yout) / 131
    gyro_z_scaled = twos_compliment(gyro_zout) / 131

    #print gyro_x_scaled

    accel_xout = (read_byte(0x3b) << 8) + read_byte(0x3c)
    accel_yout = (read_byte(0x3d) << 8) + read_byte(0x3e)
    accel_zout = (read_byte(0x3f) << 8) + read_byte(0x40)

    accel_xout_scaled = twos_compliment(accel_xout / 16384.0)
    accel_yout_scaled = twos_compliment(accel_yout / 16384.0)
    accel_zout_scaled = twos_compliment(accel_zout / 16384.0)

    rot_x = get_x_rotation(accel_xout_scaled, accel_yout_scaled, accel_zout_scaled)
    rot_y = get_y_rotation(accel_xout_scaled, accel_yout_scaled, accel_zout_scaled)
    rot_z = get_z_rotation(accel_xout_scaled, accel_yout_scaled, accel_zout_scaled)

    #print "{} -> {} -> {}".format(accel_xout_scaled, math.radians(accel_xout_scaled), rot_x)

    return rot_x, rot_y, rot_z, gyro_x_scaled, gyro_y_scaled, gyro_z_scaled


def process_data():
    pass


def twos_compliment(val):
    if val >= 0x8000:
        return -((65535 - val) + 1)
    else:
        return val


x, y, z = 0, 0, 0
polling = True
sending = True


def start_sending():
    print "starting polling..."
    global x, y, z
    global polling

    try:
        rot_x, rot_y, rot_z, gyro_x, gyro_y, gyro_z = read_gyro_data()

        last_x = rot_x
        last_y = rot_y
        last_z = rot_z

        gyro_x_offset = gyro_x
        gyro_y_offset = gyro_y
        gyro_z_offset = gyro_z

        gyro_x_total = 0 - gyro_x_offset
        gyro_y_total = 0 - gyro_y_offset
        gyro_z_total = 0 - gyro_z_offset

        while polling:
            #x, y, z, gyro_x, gyro_y, gyro_z = read_gyro_data()

            #for _ in range(0, int(3.0 / time_diff)):
            time.sleep(time_diff - 0.005)

            rot_x, rot_y, rot_z, gyro_x, gyro_y, gyro_z = read_gyro_data()

            gyro_x -= gyro_x_offset
            gyro_y -= gyro_y_offset
            gyro_z -= gyro_z_offset

            gyro_x_delta = gyro_x * time_diff
            gyro_y_delta = gyro_y * time_diff
            gyro_z_delta = gyro_z * time_diff

            gyro_x_total += gyro_x_delta
            gyro_y_total += gyro_y_delta
            gyro_z_total += gyro_z_delta

            #last_x = 0.98 * (last_x + gyro_x_delta) + (0.02 * rot_x)
            #last_y = 0.98 * (last_y + gyro_y_delta) + (0.02 * rot_y)
            #last_x += gyro_x_delta
            #last_y += gyro_y_delta

            x, y, z = gyro_x_total, gyro_y_total, gyro_z_total
            print "{0:.1f}, {1:.1f}, {2:.1f}".format(x, y, z)
            #print "{0:.1f}, {1:.1f}, {2:.1f}".format(gyro_x_offset, gyro_y_offset, gyro_z_offset)

    except KeyboardInterrupt:
        print "stopped polling..."


class MyUDPHandler(SocketServer.BaseRequestHandler):

    def handle(self):
        global sending

        data = self.request[0].strip()
        sock = self.request[1]

        print "{} wrote: ".format(self.client_address[0])
        print data

        if data.startswith("exit"):
            print "setting sending false"
            sending = False

        def send_data():
            global sending
            while sending:
                sock.sendto("{0:.1f}, {1:.1f}, {2:.1f}".format(x, y, z), self.client_address)
                #print("{0:.1f}, {1:.1f}, {2:.1f}".format(x, y, z))

                time.sleep(0.05)

            #sending = True

        Thread(target=send_data).start()


if __name__ == "__main__":
    # HOST, PORT = socket.gethostname(), 8668
    HOST, PORT = "192.168.1.14", 8668

    server = SocketServer.UDPServer((HOST, PORT), MyUDPHandler)

    t = Thread(target=start_sending)
    t.start()

    print "listening on {}".format(HOST)
    try:
        server.serve_forever()
    except KeyboardInterrupt:
        server.shutdown()

        sending = False
        polling = False
        print "stopped serving..."